#pragma once

#include "vec3.h"

namespace cgmath
{
	constexpr float pi = 3.14159226535f;

	const vec3 worldForward(0.0f, 0.0f, 1.0f);
	const vec3 worldUp(0.0f, 1.0f, 0.0f);
	const vec3 worldRight(1.0f, 0.0f, 0.0f);

	inline constexpr float radians(const float& degs)
	{
		return degs * 0.017453292f;
	}
}