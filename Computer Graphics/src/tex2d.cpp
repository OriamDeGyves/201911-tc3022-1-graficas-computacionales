#include "tex2d.h"

#include <IL/il.h>
#include <iostream>

tex2d::tex2d()
{
	_textureId = 0;
}

tex2d::~tex2d()
{
	if (_textureId)
		glDeleteTextures(1, &_textureId);
}

void tex2d::loadTexture(const std::string& path)
{
	loadTexture(path, GL_TEXTURE_2D);
}

void tex2d::loadTexture(const std::string& path, GLenum target)
{
	_target = target;

	ILuint imageId = 0;
	ilGenImages(1, &imageId);
	ilBindImage(imageId);
	if (!ilLoadImage(path.c_str()))
	{
		std::cout << "Could not load image " << path << std::endl;
	}

	glGenTextures(1, &_textureId);
	glBindTexture(_target, _textureId);
	glTexParameteri(_target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(_target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(_target, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(_target, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(_target, 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE), ilGetData());
	glBindTexture(_target, 0);

	ilBindImage(0);
	ilDeleteImages(1, &imageId);
}

void tex2d::loadTexture(const std::vector<cgmath::vec3>& pixels, int width, int height)
{
	glGenTextures(1, &_textureId);
	glBindTexture(GL_TEXTURE_2D, _textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_FLOAT, pixels.data());
	glBindTexture(GL_TEXTURE_2D, 0);
}

void tex2d::bind()
{
	glBindTexture(GL_TEXTURE_2D, _textureId);
}

void tex2d::unbind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}