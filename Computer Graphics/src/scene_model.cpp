#include "scene_model.h"

#include <IL/il.h>

#include "ifile.h"
#include "time.h"

scene_model::scene_model()
{
	sPressed = wPressed = aPressed = dPressed = qPressed = ePressed = zPressed = xPressed = false;
	renderMode = 0;
}

void scene_model::init()
{
	myModel.load("models/nanosuit.obj");

	modelShader.create();
	modelShader.attachShader("shaders/PhongTexturedShadow.vert", GL_VERTEX_SHADER);
	modelShader.attachShader("shaders/PhongTexturedShadow.frag", GL_FRAGMENT_SHADER);
	modelShader.setAttribute(0, "VertexPosition");
	modelShader.setAttribute(1, "VertexColor");
	modelShader.setAttribute(2, "VertexNormal");
	modelShader.setAttribute(3, "VertexTexCoord");
	modelShader.link();

	sceneCamera.setPosition(0.0f, 15.5f, 20.0f);
	sceneCamera.setRotation(-15.0f, 0.0f, 0.0f);

	std::vector<std::string> paths = {"images/morning_rt.tga", "images/morning_lf.tga", "images/morning_up.tga", "images/morning_dn.tga", "images/morning_ft.tga", "images/morning_bk.tga"};

	// Skybox
	cubeMapTexture = load_cube_map(paths);
	std::vector<cgmath::vec3> positions;
	std::vector<unsigned int> indices = { 0, 3, 2, 0, 2, 1, 4, 7, 6, 4, 6, 5, 8, 11, 10, 8, 10, 9, 12, 15, 14, 12, 14, 13, 16, 19, 18, 16, 18, 17, 20, 23, 22, 20, 22, 21 };

	float x = 1.0f;
	float y = 1.0f;
	float z = 1.0f;

	// Cara frontal
	positions.push_back(cgmath::vec3(-x, -y, z));
	positions.push_back(cgmath::vec3(x, -y, z));
	positions.push_back(cgmath::vec3(x, y, z));
	positions.push_back(cgmath::vec3(-x, y, z));
	// Cara derecha
	positions.push_back(cgmath::vec3(x, -y, z));
	positions.push_back(cgmath::vec3(x, -y, -z));
	positions.push_back(cgmath::vec3(x, y, -z));
	positions.push_back(cgmath::vec3(x, y, z));
	// Cara trasera
	positions.push_back(cgmath::vec3(x, -y, -z));
	positions.push_back(cgmath::vec3(-x, -y, -z));
	positions.push_back(cgmath::vec3(-x, y, -z));
	positions.push_back(cgmath::vec3(x, y, -z));
	// Cara izquierda
	positions.push_back(cgmath::vec3(-x, -y, -z));
	positions.push_back(cgmath::vec3(-x, -y, z));
	positions.push_back(cgmath::vec3(-x, y, z));
	positions.push_back(cgmath::vec3(-x, y, -z));
	// Cara superior
	positions.push_back(cgmath::vec3(-x, y, z));
	positions.push_back(cgmath::vec3(x, y, z));
	positions.push_back(cgmath::vec3(x, y, -z));
	positions.push_back(cgmath::vec3(-x, y, -z));
	// Cara inferior
	positions.push_back(cgmath::vec3(-x, -y, -z));
	positions.push_back(cgmath::vec3(x, -y, -z));
	positions.push_back(cgmath::vec3(x, -y, z));
	positions.push_back(cgmath::vec3(-x, -y, z));

	skybox.create(24);
	skybox.setAttributePosition(positions, 0);
	skybox.setIndices(indices);

	skyboxShader.create();
	skyboxShader.attachShader("shaders/Skybox.vert", GL_VERTEX_SHADER);
	skyboxShader.attachShader("shaders/Skybox.frag", GL_FRAGMENT_SHADER);
	skyboxShader.setAttribute(0, "VertexPosition");
	skyboxShader.link();
	skyboxShader.activate();
	skyboxShader.setUniformi("skybox", 0);
	skyboxShader.deactivate();

}

void scene_model::awake()
{
	glClearColor(0.96f, 0.94f, 0.95f, 1.0f);
}

void scene_model::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDepthFunc(GL_LEQUAL);
	skyboxShader.activate();
	skyboxShader.setUniformMatrix("projMatrix", sceneCamera.getProjectionMatrix());
	cgmath::mat4 view = sceneCamera.getViewMatrix();
	view[3][0] = view[3][1] = view[3][2] = 0.0f;
	skyboxShader.setUniformMatrix("viewMatrix", view);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);
	skybox.draw(GL_TRIANGLES);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	skyboxShader.deactivate();
	glDepthFunc(GL_LESS);

	myModel.modelTransform.setRotation(0.0f, 10.0f * time::elapsed_time().count(), 0.0f);

	modelShader.activate();
	modelShader.setUniformMatrix("mvpMatrix", sceneCamera.getProjectionMatrix() * sceneCamera.getViewMatrix() * myModel.modelTransform.getModelMatrix());
	modelShader.setUniformMatrix("modelMatrix", myModel.modelTransform.getModelMatrix());
	modelShader.setUniformMatrix("normalMatrix", cgmath::mat3::transpose(cgmath::mat3::inverse(cgmath::mat3(myModel.modelTransform.getModelMatrix()))));
	modelShader.setUniformf("LightPosition", -5.0f, 5.0f, 5.0f);
	modelShader.setUniformf("LightColor", 1.0f, 1.0f, 1.0f);
	modelShader.setUniformf("LightPosition", -5.0f, 5.0f, 5.0f);
	modelShader.setUniformf("CameraPosition", 0.0f, 0.0f, 3.0f);
	modelShader.setUniformi("DiffuseTexture", 0);
	modelShader.setUniformi("skybox", 1);
	modelShader.setUniformi("renderMode", renderMode);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);
	myModel.draw();
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	modelShader.deactivate();

	

	float t = time::delta_time().count();

	if (aPressed) sceneCamera.moveRight(-10.0f * t);
	if (dPressed) sceneCamera.moveRight(10.0f * t);
	if (wPressed) sceneCamera.moveForward(-10.0f * t);
	if (sPressed) sceneCamera.moveForward(10.0f * t);
	if (ePressed) sceneCamera.moveUp(10.0f * t);
	if (qPressed) sceneCamera.moveUp(-10.0f * t);
	if (zPressed) sceneCamera.yaw(10.0f * t);
	if (xPressed) sceneCamera.yaw(-10.0f * t);
}

void scene_model::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	sceneCamera.setPerspective(1.0f, 1000.0f, 60.0f, (float)width / (float)height);
}

void scene_model::normalKeysDown(unsigned char key)
{
	if (key == 'w') wPressed = true;
	if (key == 's') sPressed = true;
	if (key == 'a') aPressed = true;
	if (key == 'd') dPressed = true;
	if (key == 'q') qPressed = true;
	if (key == 'e') ePressed = true;
	if (key == 'z') zPressed = true;
	if (key == 'x') xPressed = true;

	if (key == '1') renderMode = 0;
	if (key == '2') renderMode = 1;
	if (key == '3') renderMode = 2;
}

void scene_model::normalKeysUp(unsigned char key)
{
	if (key == 'w') wPressed = false;
	if (key == 's') sPressed = false;
	if (key == 'a') aPressed = false;
	if (key == 'd') dPressed = false;
	if (key == 'q') qPressed = false;
	if (key == 'e') ePressed = false;
	if (key == 'z') zPressed = false;
	if (key == 'x') xPressed = false;
}

GLuint scene_model::load_cube_map(std::vector<std::string>& paths)
{
	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	for (unsigned int i = 0; i < paths.size(); i++)
	{
		ILuint imageID;
		ilGenImages(1, &imageID);
		ilBindImage(imageID);
		ilLoadImage(paths[i].c_str());

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE), ilGetData());
		

		ilBindImage(0);
		ilDeleteImages(1, &imageID);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}
