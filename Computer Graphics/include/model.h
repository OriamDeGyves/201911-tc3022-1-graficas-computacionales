#pragma once

#include "mesh.h"
#include "tex2d.h"
#include "transform.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <memory>
#include <vector>

class model
{
public:
	void load(const std::string& path);
	void draw();

	transform modelTransform;

private:
	void processNode(aiNode* node, const aiScene* scene);
	void processMesh(aiMesh* mesh, const aiScene* scene);

	std::vector<std::unique_ptr<mesh>> meshList;
	std::vector<std::unique_ptr<tex2d>> textureList;
};