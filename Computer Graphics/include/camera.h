#pragma once

#include "cgmath.h"
#include "mat4.h"
#include "transform.h"

class camera
{
public:
	camera();

	void setPerspective(float near, float far, float fov, float aspect);

	void setPosition(float x, float y, float z);
	void setRotation(float x, float y, float z);

	void moveForward(float delta);
	void moveRight(float delta);
	void moveUp(float delta);

	void yaw(float degrees);

	cgmath::mat4 getProjectionMatrix() const;
	cgmath::mat4 getViewMatrix() const;
private:
	cgmath::mat4 _projectionMatrix;
	cgmath::mat4 _viewMatrix;

	transform _cameraTransform;
};