#pragma once

#include "camera.h"
#include "scene.h"
#include "shader_program.h"

#include "mesh.h"
#include "model.h"

class scene_model : public scene
{
public:
	scene_model();
	~scene_model() {}

	void init();
	void awake();
	void sleep() { }
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key);
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:
	GLuint load_cube_map(std::vector<std::string>& paths);

	model myModel;
	camera sceneCamera;
	shader_program modelShader;

	mesh skybox;
	shader_program skyboxShader;
	GLuint cubeMapTexture;

	bool sPressed;
	bool wPressed;
	bool aPressed;
	bool dPressed;
	bool qPressed;
	bool ePressed;
	bool zPressed;
	bool xPressed;

	int renderMode;
};