#version 330

in vec3 InterpolatedColor;
out vec4 FragColor;

uniform vec2 resolution;

void main()
{
	vec2 p = gl_FragCoord.xy / resolution;
	vec2 center = vec2(0.5f, 0.5f);
	vec2 q = p - center;

	if (length(q) <= 0.25f)
		discard;

	FragColor = vec4(InterpolatedColor, 1.0f);
}