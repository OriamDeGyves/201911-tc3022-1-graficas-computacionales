#version 330

in vec3 VertexPosition;

out vec3 TexCoords;

uniform mat4 projMatrix;
uniform mat4 viewMatrix;

void main()
{
    TexCoords = VertexPosition;
    gl_Position = (projMatrix * viewMatrix * vec4(VertexPosition, 1.0)).xyww;
}  