#version 330

uniform float time;

out vec4 InterpolatedColor;

void main()
{
  float angleOffset = 5.0f;
  float theta = radians(mod(gl_VertexID * angleOffset, 180.0f));
  float fi = radians(mod(floor(gl_VertexID / (180.0f / angleOffset)) * angleOffset, 360.0f));
  
  float x = sin(theta) * cos(fi);
  float y = sin(theta) * sin(fi);
  float z = cos(theta);
  
  vec4 pos = vec4(x, y, z, 1.0f);
  
  mat4 rotx = mat4(1.0);
  rotx[1][1] = cos(time);
  rotx[2][1] = -sin(time);
  rotx[1][2] = sin(time);
  rotx[2][2] = cos(time);
  
  mat4 rotz = mat4(1.0);
  rotz[0][0] = cos(time);
  rotz[1][0] = -sin(time);
  rotz[0][1] = sin(time);
  rotz[1][1] = cos(time);
  
  
  gl_Position = rotz * rotx * pos;
  gl_PointSize = 2.0f;
  InterpolatedColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
}