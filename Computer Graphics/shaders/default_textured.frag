#version 330

out vec4 FragColor;

in vec2 InterpolatedTexCoord;
in vec3 PixelPosition;
in vec3 PixelNormal;

uniform vec3 LightPosition;
uniform vec3 LightColor;
uniform vec3 CameraPosition;

uniform sampler2D diffuseTexture;
uniform sampler2D decalTexture;

void main()
{
	vec3 ambient = LightColor * 0.1f;

	vec3 L = normalize(LightPosition - PixelPosition);
	vec3 diffuse = LightColor * max(0.0f, dot(L, PixelNormal));

	vec3 V = normalize(CameraPosition - PixelPosition);
	vec3 R = reflect(-L, PixelNormal);
	vec3 specular = LightColor * pow(max(0.0f, dot(V, R)), 64.0f);

	vec4 color1 = texture2D(diffuseTexture, InterpolatedTexCoord);
	vec4 color2 = texture2D(decalTexture, InterpolatedTexCoord);
	vec3 phong = (ambient + diffuse + specular) * mix(vec3(color1), vec3(color2), 0.2f);
	FragColor = vec4(phong, 1.0f);
}