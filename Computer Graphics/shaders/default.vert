#version 330

in vec3 VertexPosition;
in vec2 VertexTexCoord;
in vec3 VertexNormal;

out vec2 InterpolatedTexCoord;
out vec3 PixelPosition;
out vec3 PixelNormal;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

void main()
{
	InterpolatedTexCoord = VertexTexCoord;
	PixelPosition = vec3(modelMatrix * vec4(VertexPosition, 1.0f));
	PixelNormal = normalize(normalMatrix * VertexNormal);

	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(VertexPosition, 1.0f);
}